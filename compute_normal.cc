#include <cmath>
#include <iostream>
#include <typeinfo>
#include <string>
#include <vector>

namespace {
    struct Point3d{
        float x;
        float y;
        float z;
    };
}  // namespace

int main(int argc, char **argv) {
    if (argc != 10) { throw std::runtime_error("Specify the program file and 9 coordinates!\n"); }

    std::vector<Point3d> triangle;
    for (int idx = 0; idx < argc - 1; idx += 3) {
        triangle.push_back(Point3d({ std::stof(argv[idx + 1]), 
                                std::stof(argv[idx + 2]), 
                                std::stof(argv[idx + 3]) }));

           std::cout << "Point: { " << triangle.back().x << ", " 
                   << triangle.back().y << ", " << triangle.back().z << " }\n"; 
    }

    Point3d a = { triangle[1].x - triangle[0].x, 
            triangle[1].y - triangle[0].y,
            triangle[1].z - triangle[0].z };
    
    std::cout << "Point a: { " << a.x << ", " << a.y << ", " << a.z << " }\n";

    Point3d b = { triangle[2].x - triangle[0].x, 
            triangle[2].y - triangle[0].y,
            triangle[2].z - triangle[0].z };

    std::cout << "Point b: { " << b.x << ", " << b.y << ", " << b.z << " }\n";

    Point3d normal = { a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x };
    int len = std::sqrt(normal.x * normal.x + normal.y * normal.y + normal.z * normal.z);
    Point3d unit_normal;
    if (len > 0 ) {
        unit_normal.x = static_cast<float>(normal.x) / len;
        unit_normal.y =  static_cast<float>(normal.y) / len;
        unit_normal.z = static_cast<float>(normal.z) / len;
        std::cout << "unit normal coefficients: { " << unit_normal.x 
                << ", " << unit_normal.y << ", " << unit_normal.z << " }\n";
    }

    std::cout << "normal coefficients: { " << normal.x << ", " << normal.y << ", " << normal.z << " }\n";

    return 0;
}

